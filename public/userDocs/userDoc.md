# 内容平台媒体视频增量接口文档



| 版本号 | 时间       | 描述                                                         | 修改人 |
| ------ | ---------- | ------------------------------------------------------------ | ------ |
| V2.0   | 2022-02-07 | 基于内容平台2.3版本新增字段，新增看点、制片年份、标签、内容总时长、影片截图、获奖信息、剧集类型、内容id、上下线 | 张贺   |
| V1.0   | 2021-08-03 | 第一版同步接口                                               | 张贺   |



## 获取视频信息

请求方式：Post请求   格式：application/json

正式环境地址：<http://api-cms.trendimedia.com/external/api/videoLists](http://cms.trendymedia.com.cn:8087/external/api/videoLists>

测试环境地址：<http://staging-api-cms.trendimedia.com/external/api/videoLists>



## 请求参数

| 字段名   | 类型   | 含义                                                         | 是否必填 |
| -------- | ------ | ------------------------------------------------------------ | -------- |
| appId    | String | 应用id                                                       | 是       |
| token    | String | token，用于进行身份校验                                      | 是       |
| ts       | long   | 获取哪个时间之后修改过的视频信息，如果不填，则默认返回所有信息，毫秒级时间戳，以此来判断是全量数据还是增量数据 | 否       |
| page     | int    | 第几页数据，不填则默认第一页                                 | 否       |
| pageSize | int    | 每页包含数据条数，不填则默认每页10条数据                     | 否       |



## 响应结果

### Response对象

| 字段名   | 类型   | 说明                                     |
| -------- | ------ | ---------------------------------------- |
| status   | int    | 状态码，具体状态码见下方状态码status说明 |
| msg      | String | 信息                                     |
| pageBean | Page   | 分页信息                                 |



### Page对象

| 字段名          | 类型            | 说明                                                         |
| --------------- | --------------- | ------------------------------------------------------------ |
| lastUpdate      | long            | 该页中最后一条记录的更新时间戳，若该页没有数据，则返回所有数据中最后一次更新时间 |
| after           | long            | 该页查询的是哪个时间之后更新的，如果入参ts不为0，则该值为ts的值；如果该值为0，则为该页数据最早一条数据的更新时间 |
| allRow          | int             | 一共有多少条数据                                             |
| totalPage       | int             | 总页数                                                       |
| currentPage     | int             | 当前页                                                       |
| pageSize        | int             | 每页包含条数                                                 |
| isFirstPage     | boolean         | 是否为第一页                                                 |
| isLastPage      | boolean         | 是否为最后一页                                               |
| hasPreviousPage | boolean         | 是否有上一页                                                 |
| hasNextPage     | boolean         | 是否有下一页                                                 |
| list            | List<VideoInfo> | 视频信息数据集合                                             |



### VideoInfo对象

| 字段名           | 类型         | 说明                  | 是否必填 |
| ---------------- | ------------ | --------------------- | -------- |
| itemId           | String       | 剧集ID                | 是       |
| number           | int          | 剧集序号              | 是       |
| name             | String       | 剧集名称              | 是       |
| docName          | String       | 内容名称              | 是       |
| totalNum         | int          | 总集数                | 是       |
| location         | String       | 地区                  | 是       |
| director         | String       | 导演                  | 是       |
| type             | String       | 类型  纪录片          |          |
| description      | String       | 描述                  |          |
| publishTime      | String       | 上映年份              |          |
| keywords         | List<String> | 关键词                |          |
| hImage           | String       | 横版海报地址          |          |
| vImage           | String       | 竖版海报地址          |          |
| watchfocus       | String       | 看点                  |          |
| years            | String       | 制片年份              |          |
| tags             | List<String> | 标签                  |          |
| docTotalDuration | int          | 内容总时长(分钟)      | 是       |
| images           | List<String> | 影片截图              |          |
| rewardInfo       | String       | 获奖信息              |          |
| episodeType      | String       | 剧集类型 正片、预告片 | 是       |
| docId            | String       | 内容id                | 是       |
| online           | int          | 上下线 1.下线 2.上线  | 是       |



## 请求示例

```json
{
   "appId": "8f937b8100354b629ef0100a4af7ff7f",
   "page": 1,
   "pageSize": 10,
   "token": "7b626163eee743ba97d10af6ad5d0a8f",
   "ts": 0
}
```



## 响应示例

```json
{
   "status": 0,
        "msg": "获取成功",
        "pageBean": {
                "lastUpdate": 1627372286544,
                "after": 1627371266636,
                "allRow": 2,
                "totalPage": 1,
                "currentPage": 1,
                "pageSize": 10,
                "isFirstPage": true,
                "isLastPage": true,
                "hasPreviousPage": false,
                "hasNextPage": false,
                "list": [{
                        "itemId": "4e46452f8f5426515f160be80d01b422",
                        "number": 0,
                        "name": "异乡的戏",
                        "docName": "异乡的戏",
                        "totalNum": 2,
                        "location": "中国大陆",
                        "director": "苑皓冉,宋毅国",
                        "type": "纪录片",
                        "description": "新中国成立后，在“百花齐放，百家争鸣”的背景下，我国农村成立了一大批剧团以丰富农村的娱乐生活和宣传党的政策。但是随着时代的发展，这一些非专业的农村剧团相继解散，团员们又归田务农。本片的两位主人公就是出身于这种剧团。古稀之年的他们，为了解决现实生活中的问题，在农闲时节，他们选择重操旧业，带着一把弦子，一身行头，在广州的街头唱起故乡的调子。面对别人投递的金钱，他们认为这不是乞讨，这是一种认可，他们觉得能通过自己的本领来赚钱，改善家庭的生活，也是一件值得骄傲的事情。不过，在异乡的城市他们格格不入的工作形式也引来了很多麻烦。两位主人公也进行过是走是留的思考，他们最后如何抉择呢？",
                        "publishTime": 1604979597000,
                        "keywords": ["传统", "技·艺", "老人", "行业", "人物"],
                        "hImage": "https://rec-image.oss-cn-beijing.aliyuncs.com/20201111/9d42b915891959d5c3fa6d7b707675f3.jpg",
                        "vImage": "https://rec-image.oss-cn-beijing.aliyuncs.com/20201111/34daa9f877e1ae5c8e91e1d1d48d1b7e.jpg"
                }, {
                        "itemId": "ab5df237f81e8c6ce74e3a09f2a05770",
                        "number": 1,
                        "name": "异乡的戏",
                        "docName": "异乡的戏",
                        "totalNum": 2,
                        "location": "中国大陆",
                        "director": "苑皓冉,宋毅国",
                        "type": "纪录片",
                        "description": "新中国成立后，在“百花齐放，百家争鸣”的背景下，我国农村成立了一大批剧团以丰富农村的娱乐生活和宣传党的政策。但是随着时代的发展，这一些非专业的农村剧团相继解散，团员们又归田务农。本片的两位主人公就是出身于这种剧团。古稀之年的他们，为了解决现实生活中的问题，在农闲时节，他们选择重操旧业，带着一把弦子，一身行头，在广州的街头唱起故乡的调子。面对别人投递的金钱，他们认为这不是乞讨，这是一种认可，他们觉得能通过自己的本领来赚钱，改善家庭的生活，也是一件值得骄傲的事情。不过，在异乡的城市他们格格不入的工作形式也引来了很多麻烦。两位主人公也进行过是走是留的思考，他们最后如何抉择呢？",
                        "publishTime": "2020",
                        "keywords": ["传统", "技·艺", "老人", "行业", "人物"],
                        "hImage": "https://rec-image.oss-cn-beijing.aliyuncs.com/20201111/9d42b915891959d5c3fa6d7b707675f3.jpg",
                        "vImage": "https://rec-image.oss-cn-beijing.aliyuncs.com/20201111/34daa9f877e1ae5c8e91e1d1d48d1b7e.jpg"
                }]
                
        }
}
```



## 状态码status说明

| code | 说明       |
| ---- | ---------- |
| 0    | 成功       |
| 1    | 应用id为空 |
| 2    | 应用id错误 |
| 3    | token为空  |
| 4    | token错误  |

