// 文字将在角色列表页面同步显示
export const permissionsList = {
  'dashboard': '数据统计',
  'contents': '内容维护',
  'apps': '应用管理',
  'users': '用户管理',
  'usersDoc': '用户管理-用户手册、对接文档',
  'AppContentsCenter': "内容中心",
  'userCenter': "用户中心",
}
// console.log(`"${openPermission.map(p => p.permissionId).join('\",\"')}"`)
export const rolesPermissionList = {
  'admin': {
    'id': 'admin', // 1
    'name': '管理员',
    'describe': '用户管理',
    'permissionsName': ['dashboard', 'contents', 'apps', 'users']
  },
  'staff': {
    'id': 'staff', // 2
    'name': '内容运营',
    'describe': '运营权限',
    'permissionsName': [ 'dashboard', 'contents', 'apps', 'usersDoc']
  },
  'user': {
    'id': 'user', // 3
    'name': '开发者',
    'describe': '一般用户',
    'permissionsName': ['dashboard', 'AppContentsCenter', 'userCenter']
  }
}

export function createRoleObj(number = 1) {
  // TODO：简单的前端权限测试
  // number = 3;
  const type = ['', 'admin', 'staff', 'user'][number];
  try {
    let roleObj = {}
    for (const key in rolesPermissionList) {
      if (key === type) {
        roleObj = rolesPermissionList[key]
        break
      }
    }
    // 次级权限配置：
    let temp = {}
    roleObj.permissions = []
    roleObj.permissionsName.map(per => {
      temp = {
        'roleId': roleObj.id,
        'permissionId': per,
        'permissionName': permissionsList[per],
        'actionEntitySet': []
      }
      // ---------- 二级权限设置 ---------- //
      if (['admin'].indexOf(temp.roleId) !== -1 && temp.permissionId === 'contents') {
        temp.actionEntitySet.push({
          'action': 'contentAudit',
          'describe': '内容管理-内容审核权限'
        });
      }
      if (['staff'].indexOf(temp.roleId) !== -1 && temp.permissionId === 'contents') {
        temp.actionEntitySet.push({
          'action': 'contentOnline',
          'describe': '内容管理-内容上下线权限'
        });
      }
      if (['admin', 'staff'].indexOf(temp.roleId) !== -1 && temp.permissionId === 'apps') {
        temp.actionEntitySet.push({
          'action': 'edit',
          'describe': '应用管理-编辑应用信息'
        });
      }
      if (['user'].indexOf(temp.roleId) === -1 && temp.permissionId === 'channels') {
        temp.actionEntitySet.push({
          'action': 'delete',
          'describe': '专栏维护-删除专栏'
        });
      }
      // 格式化
      temp.actions = JSON.stringify(temp.actionEntitySet)
      roleObj.permissions.push(temp)
    })
    // 自动生成permissionList
    if (roleObj && roleObj.permissions.length > 0) {
      roleObj.permissions = roleObj.permissions
      roleObj.permissions.map(per => {
        if (per.actionEntitySet != null && per.actionEntitySet.length > 0) {
          const action = per.actionEntitySet.map(action => {
            return action.action
          })
          per.actionList = action
        }
      })
      roleObj.permissionList = roleObj.permissions.map(permission => {
        return permission.permissionId
      })
    }
    return roleObj
  } catch {
    console.error(`Error: 没有对应的角色权限配置！[${role}]`)
    new Error(`没有对应的角色权限配置！[${role}]`)
  }
}