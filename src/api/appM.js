/* ---------- 应用管理接口 ---------- */
import {
  axios
} from '@/utils/request'
import {
  map2list
} from '@/utils/common'

export function getAppMap(list) {
  /**
   * @media 媒体
   * @abut 对接方式
   * @status 合作状态
   */
  let params = {};
  list.forEach(name => {
    params[name] = true
  })
  return axios({
    url: '/app/mapping',
    params
  }).then(res => {
    if (res.status) return false;
    let map = {}
    for (let key in res.data) {
      map[key] = map2list(res.data[key])
    }
    return map
  })
}

export function getAppList(params) {
  /**
   * @name 应用名称
   * @page
   * @pageSize
   */
  return axios({
    url: "/app/appDatas",
    params
  })
}

export function addApp(params) {
  return axios({
    url: "/app/modify_app",
    method: "post",
    params
  })
}

export function getAppDetail(id) {
  return axios({
    url: "/app/appDetail/" + id,
  })
}

export function createAppToken(id) {
  return axios({
    url: "/app/generateToken",
    params: {
      id
    }
  })
}