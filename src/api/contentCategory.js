import {
  axios
} from '@/utils/request'

/* ---------- 分类列表页 ---------- */
export function getContentCategoriesList(params) {
  return axios({
    url: "/cms/recordCategories/data",
    method: "get",
    params
  })
}

export function updateContentCategoryState(params) {
  return axios({
    url: "/cms/recordCategories/delete",
    method: "get",
    params
  })
}

export function createCategory(params) {
  return axios({
    url: "/cms/recordCategories/modify_channel",
    method: "post",
    params
  })
}

/* ---------- 分类内容列表页 ---------- */
export function getCategoryContentsList(params) {
  return axios({
    url: "/cms/recordCategories/docData",
    method: "get",
    params
  })
}

export function removeContentCategory(params) {
  return axios({
    url: "/cms/recordCategories/cancel",
    method: "get",
    params
  })
}