/* ---------- 片单接口 ---------- */
import {
  axios
} from '@/utils/request'
import {
  map2list
} from '@/utils/common'

export function getAppVideoList(appId, params) {
  /**
   * @title 剧集名称
   * @status 片源状态
   */
  return axios({
    url: '/appVideo/appVideoDatas/' + appId,
    params
  })
}

export function getAppVideoMap(list) {
  /**
   * @status 片源状态
   */
  let params = {};
  list.forEach(name => {
    params[name] = true
  })
  return axios({
    url: '/appVideo/mapping',
    params
  }).then(res => {
    if (res.status) return false;
    let map = {}
    for (let key in res.data) {
      map[key] = map2list(res.data[key])
    }
    return map
  })
}

export function updateVideoStatus(ids, status) {
  return axios({
    url: "/appVideo/updateStatus",
    method: "post",
    params: {
      ids: ids.join(","),
      status
    }
  })
}

export function checkAppVideoStatus(appId) {
  return axios({
    url: "/appVideo/checkAppVideoStatus/" + appId
  })
}