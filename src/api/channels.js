/* ---------- 专栏管理接口 ---------- */
import {
  axios
} from '@/utils/request'
import {
  map2list
} from '@/utils/common'

/* ---------- 列表页 ---------- */
export function getChannelsMap(list) {
  /*
   * 获取专栏相关Map：
   * @channel 专栏
   * @title [String] 专栏名称
   * @channelType 专栏类型
   * @padding 内容填充
   * @uiStyle 排版样式
   * @channelType 内容分类
   * @includePage 所属页面
   */
  const params = {}
  list.map(name => {
    params[name] = true
  })
  return axios({
    url: "/cms/channel/mapping",
    method: "get",
    params
  }).then(res => {
    if (res.status !== 0) return;
    let map = {};
    for (let key of Object.keys(res.data)) {
      map[key] = map2list(res.data[key]);
    }
    return map
  })
}

export function getChannelsList(params) {
  return axios({
    url: "/cms/channel/alldata",
    params
  }).then(res => {
    res.pageBean.list = res.pageBean.list.map(row => {
      row.category = JSON.parse(row.category);
      row.include_page = JSON.parse(row.include_page);
      return row;
    })
    return res;
  })
}


/* ---------- 专栏操作 ---------- */
export function getChannelDetail(channelId) {
  return axios({
    url: `/cms/channel/editChannel/${channelId}`,
    method: "get",
  }).then(res => {
    res.data.category = res.data.category ? JSON.parse(res.data.category) : [];
    res.data.include_page = res.data.include_page ? JSON.parse(res.data.include_page) : [];
    return res;
  })
}

export function addChannelDetail(params) {
  return axios({
    url: "/cms/channel/modify_channel",
    method: "post",
    params
  })
}

export function uploadFile(data) {
  return axios({
    url: "/files/upload_cert",
    method: "post",
    timeout: 30000, // 请求超时时间
    data: data.file
  })
}

export function removeChannel(channelId) {
  return axios({
    url: "/cms/channel/delete",
    method: "post",
    params: {
      channelId
    }
  })
}

/* ---------- 专栏内容维护 ---------- */
export function getLinksList(params) {
  /**
   * @channel_id 专栏ID
   */
  return axios({
    url: "/cms/featured/datas",
    method: "get",
    params
  })
}

export function removeContentFromChannel(params) {
  return axios({
    url: "/cms/featuredItem/delete",
    method: "post",
    params
  })
}

export function updatePosition(params) {
  /**
   * @channelId integer
   * @docIds Array [String]
   */
  return axios({
    url: "/cms/updatePosition",
    method: "get",
    params
  })
}