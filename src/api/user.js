/* ---------- 用户管理接口 ---------- */
import {
  axios
} from '@/utils/request'
import {
  map2list
} from '@/utils/common'

// 获取用户列表
export function getUserList(params) {
  return axios({
    url: "/user/userData",
    method: "get",
    params
  })
}

// 修改用户角色
export function addUser(params) {
  /**
   * @id Number
   * @role Number
   * @user_name String
   * @nickName String
   * @phone String
   * @end_time Number
   * @apps String
   */
  return axios({
    url: "/user/modify/" + params.id,
    method: "post",
    params
  })
}

// 获取用户Map
export function getUserMaps(list) {
  /**
   * @role 角色
   * @app 应用
   */
  let params = {};
  list.forEach(name => {
    params[name] = true
  })
  return axios({
    url: "/user/mapping",
    params
  }).then(res => {
    if (res.status) return false;
    let map = {}
    for (let key in res.data) {
      map[key] = map2list(res.data[key])
    }
    return map
  })
}

// 账户邮件重置密码
export function emailToResetPassword(id) {
  return axios({
    url: "/user/reset_passwd/",
    params: {
      id
    }
  })
}

// 直接修改用户密码
export function resetPassword(userId, password) {
  return axios({
    url: "/user/updatePasswd/" + userId,
    method: "post",
    params: {
      password
    }
  })
}

// 获取单个用户详情
export function getUserInfoById(id) {
  return axios({
    url: "/user/userDetail/" + id
  })
}

// 用户审核
export function userAudit(id, auditPass) {
  /**
   * @id
   * @audit Boolean // 审核通过传1 审核不通过传2
   */
  return axios({
    url: "/user/userAudit/" + id,
    params: {
      audit: auditPass ? 1 : 2
    }
  })
}