import {
  axios
} from '@/utils/request'
import {
  map2list
} from '@/utils/common'

// 获取累计数据情况
export function getOverallData(appId, params) {
  /**
   * @appId 应用ID，管理后台为0
   * @date 时间
   */
  return axios({
    url: "/data/overview/" + appId + "/overallData",
    params
  })
}
// 获取整体播放情况
export function getOverallPlayData(appId, params) {
  /**
   * @startDate
   * @endDate
   */
  return axios({
    url: "/data/overview/" + appId + "/overallPlayData",
    params
  })
}
// 24小时播放情况
export function get24HoursData(appId, params) {
  return axios({
    url: "/data/overview/" + appId + "/hourPlayData",
    params
  })
}
// 内容标签播放占比
export function getCategoriesData(appId, params) {
  return axios({
    url: `/data/overview/${appId}/tagData`,
    params
  }).then(res => {
    if (res.data) {
      res.data = Object.keys(res.data).map(category => {
        return {
          category,
          value: res.data[category]
        }
      })
    }
    return res
  })
}
// 纪录片播放TOP100
export function getTop10Data(appId) {
  return axios({
    url: "/data/overview/" + appId + "/top10Data"
  })
}


/* ---------- 纪录片数据 ---------- */
export function getVideoDataList(appId, params) {
  /**
   * @appId
   * @itemId 剧集ID
   * @name 剧集名称
   * @page @pageSize
   */
  return axios({
    url: `/data/videoData/${appId}/list`,
    params
  })
}

/* ---------- 广告数据 ---------- */
export function getAdDataList(appId, params) {
  /**
   * @appId
   * @startDate @endDate
   * @statType 统计类型
   * @page @pageSize
   */
  return axios({
    url: `/data/${appId}/adData/list`,
    params
  })
}

export function getAdDataMaps(list) {
  /**
   * @statType 统计类型
   */
  const params = {}
  list.map(name => {
    params[name] = true
  })
  return axios({
    url: "/data/adData/mapping",
    params
  }).then(res => {
    if (res.statues) return;
    const {data} = res;
    let map = {};
    for (let key in data) {
      map[key] = map2list(data[key])
    }
    return map
  })
}