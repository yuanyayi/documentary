import {
  axios
} from '@/utils/request'
import {
  map2list
} from '@/utils/common'

/* ---------- 列表页 ---------- */
export function getContentsMap(list) {
  /*
   * 获取内容相关Map：
   * @cats 内容分类
   * @type 类型
   * @tags 标签
   */
  const params = {}
  list.map(name => {
    params[name] = true
  })
  return axios({
    url: "/cms/mapping",
    method: "post",
    params
  }).then(res => {
    if (res.status !== 0) return;
    let map = {};
    /**
     * typeMap
     * catsMap
     */
    for (let key in res.data) {
      map[key] = map2list(res.data[key]);
    }
    return map
  })
}

/*
 * 获取内容列表：
 * 可选的查询参数：
 * @type [String]: 类型: 6
 * @category_id [int]: 内容分类
 * @title [String]: 内容标题
 * @keywords [String]: 关键词
 */
export function getContentsList(params) {
  return axios({
    url: "/cms/docData",
    method: "get",
    params
  })
}

export function addColumn(params) {
  return axios({
    url: "/cms/featured/addcolumn",
    method: "post",
    params
  })
}

/* ---------- 内容操作 ---------- */
export function getContentDetail(docId) {
  return axios({
    url: `/cms/editdoc/${docId}`,
    method: "get",
  }).then(res => {
    res.data.director = res.data.director.replace(/ /g, "").split(/，|,/);
    return res;
  })
}

export function addContentDetail(params) {
  return axios({
    url: "/cms/modify_DocData",
    method: "post",
    params
  })
}

export function uploadFile(data) {
  return axios({
    url: "/files/upload_cert",
    method: "post",
    timeout: 30000, // 请求超时时间
    data: data.file
  })
}

/**
 * 剧集上下线
 * @param {string} episodeId 剧集id
 * @param {int} status 目标状态
 * @returns 
 */
export function updateEpisodeStatus(params) {
  return axios({
    url: "/cms/updateEpisodeStatus",
    method: "post",
    params
  })
}