import {
  axios
} from '@/utils/request'

/**
 * login func
 * parameter: {
 *     username: '',
 *     password: ''
 * }
 * @param parameter
 * @returns {*}
 */
export function login(params) {
  return axios({ // axios
    url: '/admin/login/',
    method: 'get',
    params: {
      ...params,
      nocache: new Date().getTime()
    }
  })
}

/**
 * getUserInfo func
 * parameter: {
 *     token: ''
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserInfo(params) {
  return axios({ // axios
    url: '/admin/userInfo/',
    method: 'get',
    params: {
      ...params,
      nocache: new Date().getTime()
    }
  })
}

export function logout() {
  return axios({
    url: '/admin/logout/',
    method: 'get'
  })
}

/**
 * login func
 * parameter: {
 *     username: '',
 *     password: ''
 * }
 * @param parameter
 * @returns {*}
 */
 export function register(params) {
   /**
    * @userName
    * @nickName
    * @phone
    */
  return axios({ // axios
    url: '/admin/register',
    method: 'get',
    params: {
      ...params,
      nocache: new Date().getTime()
    }
  })
}