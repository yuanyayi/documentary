import Line from "./Line"
import Pie from "./Pie"
import Column from "./Column"
import GroupedColumn from "./GroupedColumn"
import Bar from "./Bar"
import ChartCard from "./ChartCard"
import StackedBar from "./StackedBar"
import GroupedBar from "./GroupedBar"
import Treemap from "./Treemap"

export {
  Line as G2PlotLine,
  Pie as G2PlotPie,
  Column as G2PlotColumn,
  GroupedColumn as G2PlotGroupedColumn,
  Bar as G2PlotBar,
  GroupedBar as G2PlotGroupedBar,
  StackedBar as G2PlotStackedBar,
  Treemap as G2PlotTreemap,
  ChartCard
}