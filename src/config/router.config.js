// eslint-disable-next-line
import {
  UserLayout,
  BasicLayout,
  RouteView,
  BlankLayout,
  PageView
} from '@/layouts'
import {
  bxAnaalyse
} from '@/core/icons'

export const asyncRouterMap = [{
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: {
      title: '首页',
    },
    redirect: '/dashboard/',
    children: [
      // 数据统计
      {
        path: '/dashboard',
        component: PageView,
        redirect: '/dashboard/',
        meta: {
          title: '数据统计',
          icon: bxAnaalyse,
          permission: ['dashboard']
        },
        children: [{
          path: '/dashboard/',
          name: 'dashboard',
          component: () => import('@/views/dashboard/'),
          meta: {
            title: '数据概览'
          }
        }, {
          path: '/dashboard/videoData',
          name: 'videoData',
          component: () => import('@/views/dashboard/videoData'),
          meta: {
            title: '纪录片数据'
          }
        }, {
          path: '/dashboard/adData',
          name: 'adData',
          component: () => import('@/views/dashboard/adData'),
          meta: {
            title: '广告数据'
          }
        }],
      },
      // 内容维护
      {
        path: '/contents',
        component: PageView,
        redirect: '/contents/',
        meta: {
          title: '内容维护',
          icon: 'file-text',
          permission: ['contents']
        },
        children: [{
            path: '/contents/',
            name: 'contents',
            component: () => import('@/views/contentsManagement/'),
            meta: {
              title: '内容列表'
            }
          },
          {
            path: '/contents/:id?',
            name: 'contentDetail',
            component: () => import('@/views/contentsManagement/contentDetail'),
            hidden: true,
            meta: {
              title: '内容详情'
            }
          }
        ]
      },
      // 应用管理
      {
        path: '/apps',
        component: PageView,
        redirect: '/apps/',
        meta: {
          title: '应用管理',
          icon: 'desktop',
          permission: ['apps']
        },
        children: [{
          path: '/apps/',
          name: 'apps',
          component: () => import('@/views/appsManagement/'),
          meta: {
            title: '应用列表'
          }
        }, {
          path: '/apps/:id([1-9]\\d*)?',
          name: 'appDetail',
          component: () => import('@/views/appsManagement/appDetail'),
          hidden: true,
          meta: {
            title: '应用详情'
          }
        }]
      },
      // 用户管理
      {
        path: '/userM',
        component: PageView,
        redirect: '/userM/userList',
        meta: {
          title: '用户管理',
          icon: 'user-o',
          permission: ['users', 'usersDoc']
        },
        children: [{
            path: '/userM/userList',
            component: () => import('@/views/userManagement/userList'),
            name: "userList",
            meta: {
              title: '用户列表',
              permission: ['users']
            },
          },
          {
            path: '/userM/roleList',
            component: () => import('@/views/userManagement/roleList'),
            name: "roleList",
            meta: {
              title: '角色列表',
              permission: ['users']
            },
          },
          {
            path: '/userM/userDocs',
            component: () => import('@/views/userDocs'),
            name: "userDocs",
            meta: {
              title: '用户手册',
              permission: ['users', 'usersDoc']
            },
          },
          {
            path: '/userM/docking',
            component: () => import('@/views/userDocs'),
            name: "dockingDocs",
            meta: {
              title: '对接文档',
              permission: ['users', 'usersDoc']
            },
          }
        ]
      },
      /* ---------- 用户页面组 ---------- */
      // 内容中心
      {
        path: '/appContentsCenter',
        component: PageView,
        redirect: '/appContentsCenter/',
        meta: {
          title: '内容中心',
          icon: 'file-text',
          permission: ['AppContentsCenter'],
          keepAlive: false
        },
        children: [{
          path: '/appContentsCenter/',
          name: 'AppContentsCenter',
          component: () => import('@/views/AppContentsCenter/'),
          meta: {
            title: '内容详情'
          }
        }]
      },
      // 用户中心
      {
        path: '/userCenter',
        component: PageView,
        redirect: '/userCenter/',
        meta: {
          title: '用户中心',
          icon: 'user-o',
          permission: ['userCenter']
        },
        children: [{
          path: '/userCenter/',
          name: "userCenter",
          component: () => import('@/views/UserCenter/'),
          meta: {
            title: '用户详情',
          }
        }, {
          path: '/userM/userDocs',
          component: () => import('@/views/userDocs'),
          name: "userDocs",
          meta: {
            title: '用户手册',
          },
        }, {
          path: '/userM/docking',
          component: () => import('@/views/userDocs'),
          name: "dockingDocs",
          meta: {
            title: '对接文档',
          },
        }]
      }
    ]
  },
  {
    path: '/test',
    name: 'test',
    component: () => import('@/views/test/'),
    hidden: true
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [{
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [{
        path: 'login',
        name: 'login',
        component: () => import( /* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import( /* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import( /* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      }
    ]
  },
  {
    path: '/404',
    component: () => import( /* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]