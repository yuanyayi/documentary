import Vue from 'vue'
import Router from 'vue-router'
import {
  constantRouterMap
} from '@/config/router.config'

Vue.use(Router)


const createRouter = () => new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
});

let ROUTETREE = createRouter();
// new Router({
//   // mode: 'history',
//   base: process.env.BASE_URL,
//   scrollBehavior: () => ({
//     y: 0
//   }),
//   routes: constantRouterMap
// })

export default ROUTETREE;

export function resetRoutes(newRoutes) {
  ROUTETREE.matcher = createRouter().matcher;
	ROUTETREE.addRoutes(newRoutes);
}