const getters = {
  device: state => state.app.device,
  theme: state => state.app.theme,
  color: state => state.app.color,
  token: state => state.user.token,
  nickname: state => state.user.name,
  userid: state => state.user.id,
  roles: state => state.user.roles,
  role: state => state.user.roles[0],
  addRouters: state => state.permission.addRouters,
  multiTab: state => state.app.multiTab,
  userInfo: state => state.user.userInfo,
  appId: state=>state.user.appId
}

export default getters