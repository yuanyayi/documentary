import Vue from 'vue'
import {
  login,
  logout,
  getUserInfo
} from '@/api/login'
import {
  ACCESS_TOKEN
} from '@/store/mutation-types'
import {
  createRoleObj
} from "@/rolesPermissionList"

const user = {
  state: {
    roles: [],
    userInfo: {},
    appId: 0
  },

  mutations: {
    SET_ROLES: (state, roles) => {
      state.roles = [roles]
    },
    SET_USERINFO: (state, info) => {
      state.userInfo = info
    },
    SET_APPID: (state, appId) => {
      state.appId = appId
    }
  },

  actions: {
    // 登录
    Login({
      commit
    }, userInfo) {
      return login(userInfo).then(response => {
        if (+response.status !== 0) {
          throw response
        }
        const token = response.token || Math.floor(Math.random() * 10 * 8)
        Vue.ls.set(ACCESS_TOKEN, token, 7 * 24 * 60 * 60 * 1000)
      })
    },

    // 获取用户信息
    GetInfo({
      commit
    }) {
      if (Vue.ls.get(ACCESS_TOKEN)) {
        return getUserInfo().then(res => {
          commit('SET_USERINFO', res.data)
          const appId = Vue.ls.get("APPID");
          commit('SET_APPID', appId)
          const role = createRoleObj(res.data.type || 3);
          commit('SET_ROLES', role);
          process.env.NODE_ENV !== 'production' ? console.log(role) : null;
          return {
            // 控制路由表
            roles: appId ? createRoleObj(3) : role,
          }
        })
      }
    },

    // 登出
    Logout({
      commit
    }) {
      return new Promise((resolve, reject) => {
        commit('SET_ROLES', [])
        commit('SET_USERINFO', {})
        Vue.ls.remove(ACCESS_TOKEN)
        commit('SET_APPID', 0)
        Vue.ls.set("APPID", 0)
        logout().then(() => {
          resolve()
        }, () => {
          reject()
        })
      })
    },

    // 更新APPID
    UpdateAPPID({
      commit
    }, appId) {
      if (appId.toString().match(/^userType(\d*)/)) {
        appId = 0;
      }
      commit('SET_APPID', appId);
      Vue.ls.set("APPID", appId)
    }
  }
}

export default user