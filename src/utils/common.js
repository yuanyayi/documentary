import moment from "moment"

// 从列表中转vals为Label, 要求value格式完全一致才可计算
export function readFromList(vals, list, char = '，', emptytxt = "<无>", opt) {
  char = !!char ? char : '，';
  const label = (opt && opt.label) || "label"
  // value => label
  if (([undefined, null, ""].indexOf(vals) !== -1) || ([undefined, null, ""].indexOf(list) !== -1) || !list.length) {
    if (process.env.NODE_ENV !== 'production') {
      console.error("readFromList方法传入参数有误，重新检查代码。")
      console.error(vals, list);
    }
    return emptytxt
  }
  if (!(vals instanceof Array)) {
    vals = [vals.toString()];
  }
  if (!vals.length) return emptytxt
  const result = list
    .filter(el => {
      return (vals.indexOf(el.value) !== -1) || (vals.indexOf('' + el.value) !== -1)
    })
    .map(el => el[label] || el.label)
  return result.length > 0 ? result.join(char) : emptytxt
}

// 千分位标记法
export function toThousands(num) {
  if (!num && num !== 0) return;
  if (isNaN(num)) {
    console.error("千分位标记法：输入不能转化为一个合法数字。" + num)
    return num
  }
  return `${num}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}
export function parseThousands(str) {
  return str.replace(/\$\s?|(,*)/g, '')
}

// 计算百分比
export function divisionFilter(a, b, fixed = 2) {
  if (a === 0) {
    return "0%";
  } else if (b === 0) {
    return "100%";
  } else {
    return ((a / b) * 100).toFixed(fixed) + "%";
  }
}

// 混合对象
export function mixinObj(o1, o2) {
  let floor = 0
  for (const k in o2) {
    floor = 0
    if (o2[k] instanceof Array) {
      o1[k] = [].concat(o2[k])
    } else if (typeof o2[k] !== 'object') {
      o1[k] = o2[k]
    } else {
      floor++
      o1[k] = o1[k] || {}
      if (floor < 5) {
        mixinObj(o1[k], o2[k])
      } else {
        try {
          o1[k] = Object.assign({}, o1[k], o2[k])
        } catch (err) {
          o1[k] = o2[k]
        }
      }
    }
  }
  return o1
}

export function formatDate(timestamp) {
  try {
    return timestamp ? moment(+timestamp).format("YYYY/MM/DD") : "无";
  } catch (err) {
    console.error("错误的时间戳格式：");
    console.error(err);
    return "无";
  }
}

export function formatTime(timestamp) {
  try {
    return timestamp ? moment(+timestamp).format("YYYY/MM/DD HH:mm:ss") : "无";
  } catch (err) {
    console.error("错误的时间戳格式：");
    console.error(err);
    return "无";
  }
}

// MAP转化为LIST
export function map2list(map, type = 'kv', keyTypeIsNumber = true) {
  const arr = []
  for (const key in map) {
    if (type === 'kv') {
      arr.push({
        label: map[key],
        value: keyTypeIsNumber ? +key : key.toString()
      })
    } else {
      arr.push({
        label: key,
        value: keyTypeIsNumber ? +map[key] : map[key].toString()
      })
    }
  }
  return arr
}

// 获取文件名
export function fileNameFilter(url) {
  const temp = /[A-Za-z0-9_\-]*\.[A-Za-z0-9_\-]*$/.exec(url);
  return temp ? temp.join("") : "素材";
}

// 格式化上传文件列表
export function formatFileList(fileList, limit) {
  let list = fileList.map(file => {
    return file.url || file.response.data.url;
  });
  return limit === 1 ? list.join() : JSON.stringify(list);
}

/**
 * 图表图片导出
 * @param chart chart 实例
 * @param name 图片名称，可选，默认名为 'G2Chart'
 */
export function downloadImage(chart, name = 'G2Chart') {
  const download = () => {
    const element = chart.container.querySelector("canvas");
    const newCanvas = document.createElement("canvas");
    newCanvas.width = element.width + 40;
    newCanvas.height = element.height + 40
    newCanvas.id = "newCanvas";

    // document.querySelector("#app").appendChild(newCanvas);
    const ctx = newCanvas.getContext('2d');
    ctx.fillStyle = "#fff";
    ctx.fillRect(0, 0, newCanvas.width, newCanvas.height);
    ctx.drawImage(element, 20, 20, newCanvas.width - 20, newCanvas.height - 20)


    const imgData = newCanvas.toDataURL("image/png")
    fileDownload(imgData);
  };
  const fileDownload = (imgData) => {
    const aLink = document.createElement('a');
    aLink.style.display = 'none';
    aLink.href = imgData;
    aLink.download = name + '.png';
    // 触发点击-然后移除
    document.body.appendChild(aLink);
    aLink.click();
    document.body.removeChild(aLink);
  };
  return download()
}

export function fileDownload(href, download) {
  const downloadLink = document.createElement('a')
  downloadLink.href = href
  downloadLink.download = download || href;
  downloadLink.click()
}