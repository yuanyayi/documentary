import Vue from 'vue'
import axios from 'axios'
import store from '@/store'
import notification from 'ant-design-vue/es/notification'
import {
  VueAxios
} from './axios'
import {
  ACCESS_TOKEN
} from '@/store/mutation-types'

// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL, // api base_url
  withCredentials: true, // send cookies when cross-domain requests
  headers: {
    'content-type': 'application/json'
  }, // CORS
  // timeout: 40000 // 请求超时时间
})

const err = (error) => {
  if (error.response) {
    const res = error.response.data
    const token = Vue.ls.get(ACCESS_TOKEN)

    if (error.response.status == 1) {
      notification.error({
        message: '请求错误',
        description: res.msg
      })
    }

    if (error.response.status == 2) {
      showLogoutNote && notification.error({
        message: '账号验证异常',
        description: res.msg || '账号过期或没有权限，请重新登录！'
      })
      holdAnotherNotification();
      if (token) {
        store.dispatch('Logout').finally(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    }
  }
  return Promise.reject(error)
}

// 后台
// request interceptor
service.interceptors.request.use(config => {
  const token = Vue.ls.get(ACCESS_TOKEN)
  if (token) {
    // config.headers['Access-Token'] = token // 让每个请求携带自定义 token 请根据实际情况自行修改
    config.params = Object.assign({}, config.params, {
      token
    })
  }
  return config
}, err)

// response interceptor
service.interceptors.response.use((response) => {
  if ([0, 1].indexOf(+response.data.status) === -1) {
    notification.error({
      message: '请求错误',
      description: response.msg || '请求出错，即将重新登录。。。'
    })
    store.dispatch('Logout').finally(() => {
      setTimeout(() => {
        window.location.reload()
      }, 1500)
    })
  }
  /* response.data.status：
   * 0: 正常 1: 请求异常 3: 账号异常
   */
  return response.data
})

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service)
  }
}

export {
  installer as VueAxios,
  service as axios
}